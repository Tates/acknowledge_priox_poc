﻿using Priox.Core.Models.Interfaces;
using Priox.Core.Models.Interfaces.UIComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Priox.Server.WebAPI.Controllers
{
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {
        [HttpGet()]
        [Route("user/{userName}/{passwordHash}")]
        public IPxUser GetUser(string userName, string passwordHash)
        {
            try
            {
                var hash = HttpServerUtility.UrlTokenDecode(passwordHash);
                return Priox.Core.Server.Screens.LoginScreen.Login(userName, hash);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // [HttpGet("{userName}")]
        [HttpGet]
        [Route("salt/{name}")]
        public string GetUserSalt(string name)
        {
            return HttpServerUtility.UrlTokenEncode(Priox.Core.Server.Screens.LoginScreen.GetUserSalt(name));
        }

        [HttpGet]
        [Route("")]
        public IPxWindow GetWindow()
        {
            return Priox.Core.Server.Screens.LoginScreen.GetWindow();
        }
    }
}
