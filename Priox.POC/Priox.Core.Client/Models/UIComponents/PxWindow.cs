﻿using Priox.Core.Models.Interfaces.UIComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Client.Models.UIComponents
{
    public class PxWindow : IPxWindow
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public string Name { get; set; }
        public ComponentType Type => ComponentType.Window;

        public PxComponentCollection Children { get; set; }
        public string Text { get; set; }

        public void Dispose() { }
    }
}
