﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Priox.Core.Logic
{
    public static class EncriptionTools
    {
        private static Random rand = new Random();
        public static byte[] GetRandomSalt()
        {
            byte[] arr = new byte[127];
            rand.NextBytes(arr);
            return arr;
        }

        public static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            List<byte> plainTextWithSaltBytes = new List<byte>(plainText.Length + salt.Length);
            plainTextWithSaltBytes.AddRange(plainText);
            plainTextWithSaltBytes.AddRange(salt);
            return algorithm.ComputeHash(plainTextWithSaltBytes.ToArray());
        }

        public static bool CompareByteArrays(byte[] array1, byte[] array2)
        {
            if (array1.Length != array2.Length)
                return false;
            for (int i = 0; i < array1.Length; i++)
                if (array1[i] != array2[i]) return false;
            return true;
        }
    }
}
