﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Priox.Server.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Routes.MapHttpRoute(
                name: "ExtendedApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { action="get", id = RouteParameter.Optional }
            );
        }
    }
}
