﻿using System;
using System.Collections.Generic;
using System.Text;
using Priox.Core.Models.Interfaces.UIComponents;

namespace Priox.Core.Client.Models.UIComponents
{
    public class PxTextBox : IPxTextBox
    {
        public string Text { get; set; }
        public char PasswordChar { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public ComponentType Type => ComponentType.TextBox;

        public string Name { get; set; }

        public PxComponentCollection Children { get; set; }
        public void Dispose() { }
    }
}
