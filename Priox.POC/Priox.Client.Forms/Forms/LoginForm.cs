﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Priox.Core.Client.WebAPI.Screens;
using Priox.Core.Client.Screens;
using Priox.Client.Forms.Tools;
using System.Threading;
using Priox.Core.Models.Interfaces;

namespace Priox.Client.Forms.Forms
{
    public partial class LoginForm : Form
    {
        private const string serverUri = "http://localhost:49856/api";
        private ILoginScreen _screen;
        bool shouldWait = true;
        public LoginForm()
        {
            InitializeComponent();
            FillControls();
        }

        private void FillControls()
        {
            new Thread(() =>
            {
                _screen = new LoginScreen($"{serverUri}/Login");
                foreach (var comp in _screen.Window.Children)
                {
                    this.Invoke((Action)(() =>
                    {
                        Control c = comp.ToFormControl();
                        if (c != null)
                        {
                            ConfigControl(c);
                            this.Controls.Add(c);
                        }
                    }));
                }
                shouldWait = false;
            }).Start();
        }

        private async Task LoginAsync()
        {
            if (!Controls.ContainsKey("tbxUserName")
                || !Controls.ContainsKey("tbxPassword")) return;
            TextBox tbxUserName = Controls.Find("tbxUserName", true).First() as TextBox;
            TextBox tbxPassword = Controls.Find("tbxPassword", true).First() as TextBox;
            IPxUser user = await _screen.LoginAsync(tbxUserName.Text, tbxPassword.Text);
            if (user != null)
            {
                MessageBox.Show($"You have been logged in as {user.UserName}");
                ExampleForm newForm = new ExampleForm();
                this.Hide();
                newForm.ShowDialog(this);
                this.Close();
            }
        }

        private void ConfigControl(Control c)
        {
            switch (c.Name)
            {
                case "tbxUserName":
                    {
                        if (c is TextBox tbxUserName)
                        {

                        }
                    }
                    break;
                case "tbxPassword":
                    {
                        if (c is TextBox tbxPassword)
                        {

                        }
                    }
                    break;
                case "btnLogin":
                    {
                        if (c is Button btn)
                        {
                            btn.Click += async (o, e) => await this.LoginAsync();
                        }
                    }
                    break;
                case "btnCancel":
                    {
                        if (c is Button btn)
                        {
                            btn.Click += (o, e) => this.Close();
                        }
                    }
                    break;
            }
        }
    }
}
