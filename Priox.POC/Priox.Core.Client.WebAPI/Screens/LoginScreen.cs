﻿using Newtonsoft.Json;
using Priox.Core.Client.Screens;
using Priox.Core.Models.Interfaces;
using Priox.Core.Models.Interfaces.UIComponents;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Priox.Core.Client.WebAPI.Screens
{
    public class LoginScreen : ILoginScreen
    {
        //uri=localhost:8860/api/Login
        private HttpClient client = new HttpClient();
        private string _url;
        public IPxWindow Window { get; private set; }
        public LoginScreen(string url)
        {
            _url = url;
            Window = GetWindowAsync().Result;
        }

        private async Task<IPxWindow> GetWindowAsync()
        {
            return await Tools.APITools.GetObjectAsync<Client.Models.UIComponents.PxWindow>(_url);
        }

        public async Task<IPxUser> LoginAsync(string userName, string password)
        {
            byte[] salt = await GetSaltAsync(userName);
            byte[] hash = Core.Logic.EncriptionTools.GenerateSaltedHash(Encoding.UTF8.GetBytes(password), salt);
            return await GetUser(userName, hash);
        }

        public async Task<IPxUser> GetUser(string userName, byte[] passwordHash)
        {
            return await Tools.APITools.GetObjectAsync<Client.Models.PxUser>($"{_url}/user/{userName}/{HttpServerUtility.UrlTokenEncode(passwordHash)}");
        }

        public async Task<byte[]> GetSaltAsync(string userName)
        {
            string result = await Tools.APITools.GetStringAsync($"{_url}/salt/{userName}");
            return HttpServerUtility.UrlTokenDecode(result);
        }

    }
}
