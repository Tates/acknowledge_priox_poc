﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Models.Interfaces.UIComponents
{
    public interface IPxWindow : IPxComponent
    {
        string Text { get; set; }
    }
}
