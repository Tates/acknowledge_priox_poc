﻿using Priox.Core.Models.Interfaces.UIComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Server.Models.UIComponents
{
    public class PxWindow : IPxWindow
    {
        public string Text { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }

        public PxComponentCollection Children { get; set; } = new PxComponentCollection();
        public ComponentType Type => ComponentType.Window;
        public PxWindow(string name, int x, int y, int width, int height, string text)
        {
            Text = text;
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Name = name;
        }

        public void Dispose() { }
    }
}
