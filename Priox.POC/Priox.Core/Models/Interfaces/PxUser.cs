﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Models.Interfaces
{
    public interface IPxUser
    {
        string UserName { get; }
        PxUserLevel Level { get; }
    }
    public enum PxUserLevel
    {
        ADMIN = 1,
        MOD = 2,
        USER = 3,
        VIEWER = 4
    }
}
