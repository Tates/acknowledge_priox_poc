﻿using Priox.Core.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Server.Database.Screens
{
    public class LoginScreenDB : IDisposable
    {
        PxDatabaseConnect dbConnection = new PxDatabaseConnect();

        public void Dispose()
        {
            dbConnection.Dispose();
        }

        public byte[] GetSalt(string userName)
        {
            string query = dbConnection.GetQuery("Login", "GetSalt");
            using (var reader = dbConnection.ExecuteQuery(query, new object[] { userName }))
            {
                reader.Read();
                return System.Web.HttpServerUtility.UrlTokenDecode((string)reader[0]);
            }
        }

        public IPxUser Login(string userName, byte[] passwordHash)
        {
            Models.PxUser user = null;
            PxUserLevel lvl;
            string query = dbConnection.GetQuery("Login", "Login");
            using (var reader = dbConnection.ExecuteQuery(query, new object[] { userName, System.Web.HttpServerUtility.UrlTokenEncode(passwordHash) }))
            {//user_name, user_level
                if (!reader.Read()) return null;
                if (!reader.GetString(0).Equals(userName)) return null;
                lvl = (PxUserLevel)reader.GetInt32(1);
            }
            user = new Models.PxUser(userName, lvl);
            return user;
        }
    }
}
