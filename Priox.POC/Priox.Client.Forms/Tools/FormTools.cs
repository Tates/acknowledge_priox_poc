﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priox.Client.Forms.Tools
{
    public static class FormTools
    {
        public static TextBox ToFormControl(this Core.Models.Interfaces.UIComponents.IPxTextBox setting)
        {
            TextBox textBox = new TextBox()
            {
                Location = new System.Drawing.Point(setting.X, setting.Y),
                Name = setting.Name,
                Size = new System.Drawing.Size(setting.Width, setting.Height),
                TabIndex = 0,
                PasswordChar = setting.PasswordChar,
                Text = setting.Text
            };
            return textBox;
        }

        public static Button ToFormControl(this Core.Models.Interfaces.UIComponents.IPxButton setting)
        {
            Button btn = new Button()
            {
                Location = new System.Drawing.Point(setting.X, setting.Y),
                Name = setting.Name,
                Size = new System.Drawing.Size(setting.Width, setting.Height),
                TabIndex = 0,
                Text = setting.Text,
                UseVisualStyleBackColor = true
            };
            return btn;
        }

        public static Control ToFormControl(this Core.Models.Interfaces.UIComponents.IPxComponent setting)
        {
            switch (setting.Type)
            {
                case Core.Models.Interfaces.UIComponents.ComponentType.Button:
                    return ((Core.Models.Interfaces.UIComponents.IPxButton)setting).ToFormControl();
                case Core.Models.Interfaces.UIComponents.ComponentType.TextBox:
                    return ((Core.Models.Interfaces.UIComponents.IPxTextBox)setting).ToFormControl();
                default:
                    return null;
            }
        }

    }
}
