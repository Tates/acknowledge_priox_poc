﻿CREATE TABLE PxUser (
	user_name TEXT(127) NOT NULL,
	password_hash TEXT(256) NOT NULL,
	salt TEXT(256) NOT NULL,
	user_level INTEGER DEFAULT 4 -- 1=admin, 2=mod, 3=user, 4=viewer
);
