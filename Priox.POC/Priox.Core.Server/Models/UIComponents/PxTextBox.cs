﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priox.Core.Models.Interfaces.UIComponents;

namespace Priox.Core.Server.Models.UIComponents
{
    public class PxTextBox : IPxTextBox
    {
        public string Text { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public PxComponentCollection Children { get; set; } = new PxComponentCollection();
        public char PasswordChar { get; set; } = '\0';
        public ComponentType Type => ComponentType.TextBox;

        public PxTextBox(string name)
        {
            Name = name;
        }

        public void Dispose() { }
    }
}
