﻿using Priox.Core.Models.Interfaces;
using Priox.Core.Models.Interfaces.UIComponents;
using Priox.Core.Server.Models.UIComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Server.Screens
{
    public class LoginScreen
    {
        private const int padding = 8;
        public static IPxWindow GetWindow()
        {
            IPxWindow window = new PxWindow("LoginWindow", 0, 0, 1028, 720, "Login screen");
            window.Children.Add(new PxTextBox("tbxUserName") { X = padding, Y = padding, Width = 240, Height = 24, Text = string.Empty });
            window.Children.Add(new PxTextBox("tbxPassword") { X = padding, Y = padding * 2 + 24, Width = 240, Height = 24, Text = string.Empty, PasswordChar = '*' });
            window.Children.Add(new PxButton("btnLogin", padding, padding * 3 + 24 * 2, 120, 24, "Login"));
            window.Children.Add(new PxButton("btnCancel", padding * 2 + 120, padding * 3 + 24 * 2, 120, 24, "Cancel"));
            return window;
        }

        public static byte[] GetUserSalt(string userName)
        {
            using (Database.Screens.LoginScreenDB db = new Database.Screens.LoginScreenDB())
            {
                return db.GetSalt(userName);
            }
        }

        public static IPxUser Login(string userName, byte[] passwordHash)
        {
            //compare hash in database
            using (Database.Screens.LoginScreenDB db = new Database.Screens.LoginScreenDB())
                return db.Login(userName, passwordHash);
        }
    }
}
