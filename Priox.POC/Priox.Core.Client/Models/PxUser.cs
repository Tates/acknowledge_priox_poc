﻿using Priox.Core.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Client.Models
{
    public class PxUser : IPxUser
    {
        public string UserName { get; set; }

        public PxUserLevel Level { get; set; }
    }
}
