﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Priox.Core.Models.Interfaces.UIComponents
{
    public interface IPxButton : IPxComponent
    {
        string Text { get; set; }
    }
}
