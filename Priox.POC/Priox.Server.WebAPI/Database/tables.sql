﻿CREATE TABLE PxUser (
	user_name TEXT(127) NOT NULL,
	password_hash BLOB(512) NOT NULL,
	salt BLOB(127) NOT NULL,
	user_level INTEGER DEFAULT 4 -- 1=admin, 2=mod, 3=user, 4=viewer
);
