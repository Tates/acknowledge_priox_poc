﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Priox.Core.Models.Interfaces.UIComponents
{
    public abstract class PxComponent : IPxComponent
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public PxComponentCollection Children { get; set; }
        public ComponentType Type => ComponentType.Button;
        public PxComponent(string name, int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Name = name;
        }

        public abstract void Dispose();
    }

    public interface IPxComponent : IDisposable
    {
        ComponentType Type { get; }
        int X { get; set; }
        int Y { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        string Name { get; set; }
        PxComponentCollection Children { get; }
    }

    public enum ComponentType
    {
        Component,
        Window,
        TextBox,
        Button
    }

    public class PxComponentCollection : ICollection<IPxComponent>
    {
        public IDictionary<string, IPxComponent> Components { get; } = new Dictionary<string, IPxComponent>();

        public int Count => Components.Count;

        public bool IsReadOnly { get; protected set; } = false;

        public void Add(IPxComponent item) => Components.Add(item.Name, item);

        public void Clear() => Components.Clear();

        public bool Contains(IPxComponent item) => Components.ContainsKey(item.Name);

        public void CopyTo(IPxComponent[] array, int arrayIndex) => Components.Values.CopyTo(array, arrayIndex);

        public IEnumerator<IPxComponent> GetEnumerator() => Components.Values.GetEnumerator();

        public bool Remove(IPxComponent item) => Components.Remove(item.Name);

        IEnumerator IEnumerable.GetEnumerator() => Components.GetEnumerator();
    }
}
