﻿using Priox.Core.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Client.Screens
{
    public interface IExampleScreen
    {
        IPxUser User { get; }
    }
}
