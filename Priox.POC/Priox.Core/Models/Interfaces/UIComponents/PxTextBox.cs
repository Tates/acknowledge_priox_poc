﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Priox.Core.Models.Interfaces.UIComponents
{
    public interface IPxTextBox : IPxComponent
    {
        string Text { get; set; }
        char PasswordChar { get; set; }
    }
}
