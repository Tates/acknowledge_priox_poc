﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Priox.Core.Client.WebAPI.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Client.WebAPI.Screens.Tests
{
    [TestClass()]
    public class LoginTest
    {
        private string apiUrl = "http://localhost:49856/api";

        [TestMethod()]
        public void LoginScreenTest()
        {
            var screen = new Screens.LoginScreen($"{apiUrl}/Login/");
            Assert.IsNotNull(screen?.Window?.Children);
        }

        [TestMethod()]
        public void GetSaltTest()
        {
            var screen = new Screens.LoginScreen($"{apiUrl}/Login/");
            Assert.IsNotNull(screen?.Window);
            byte[] result = screen.GetSaltAsync("admin").Result;
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void GetUserTest()
        {
            var screen = new Screens.LoginScreen($"{apiUrl}/Login/");
            Assert.IsNotNull(screen?.Window);
            var user = screen.LoginAsync("admin", "password").Result;
            Assert.IsFalse(string.IsNullOrEmpty(user?.UserName));

        }
    }
}