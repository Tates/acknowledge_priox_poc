﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Web;
using IniParser;
using IniParser.Model;
using Priox.Core.Logic;

namespace Priox.Core.Server.Database
{
    public class PxDatabaseConnect : IDisposable
    {
        private const string DBRootPath = @"C:\Users\Tates\git\acknowledge_priox_poc\Priox.POC\Database";
        private string dbFilePath = $@"{DBRootPath}\db.sqlite";
        private string queryDataPath = $@"{DBRootPath}\DatabaseLookup.ini";
        private SQLiteConnection m_dbConnection;
        private IniData queries;
        public PxDatabaseConnect()
        {
            FileIniDataParser iniParser = new FileIniDataParser();
            DirectoryInfo dir = new DirectoryInfo(DBRootPath);
            try
            {
                dir.Parent.Attributes = FileAttributes.Normal;
                if (!dir.Exists)
                    dir.Create();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            queries = iniParser.ReadFile(queryDataPath);
            ConnectDatabase();
        }

        private void ConnectDatabase()
        {
            if (!File.Exists(dbFilePath))
                GenerateAndConnectToDatabase();
            m_dbConnection = new SQLiteConnection($"Data Source={dbFilePath};Version=3;");
            m_dbConnection.Open();
        }

        private void GenerateAndConnectToDatabase()
        {
            try
            {
                SQLiteConnection.CreateFile(dbFilePath);
                m_dbConnection = new SQLiteConnection($"Data Source={dbFilePath};Version=3;");
                m_dbConnection.Open();
                StringBuilder databaseScript = new StringBuilder();
                using (Stream str = new FileStream($@"{DBRootPath}\tables.sql", FileMode.Open))
                using (StreamReader sr = new StreamReader(str))
                {
                    while (!sr.EndOfStream)
                        databaseScript.Append(sr.ReadLine()).Append(Environment.NewLine);
                }
                ExecuteNonQuery(databaseScript.ToString());
                // new user
                {
                    byte[] salt = EncriptionTools.GetRandomSalt();
                    byte[] password = EncriptionTools.GenerateSaltedHash(Encoding.UTF8.GetBytes(@"password"), salt);

                    ExecuteNonQuery(queries["Register"]["RegisterUser"], new object[] {
                        "admin",
                        HttpServerUtility.UrlTokenEncode(password),
                        HttpServerUtility.UrlTokenEncode(salt),
                        1
                    });
                }
            }
            catch (Exception ex)
            {
                if (File.Exists(dbFilePath))
                    File.Delete(dbFilePath);
                throw ex;
            }
        }

        public DbDataReader ExecuteQuery(string sqlQuery, IEnumerable<object> parameters)
        {
            var command = new SQLiteCommand(sqlQuery, m_dbConnection);
            string paramName = "@param";
            int paramNr = 1;
            foreach (object o in parameters)
            {
                string parNam = paramName + paramNr;
                paramNr++;
                if (!sqlQuery.Contains(parNam)) throw new PxSQLException("Query does not contain a parameter with this name.");
                command.Parameters.Add(new SQLiteParameter(parNam, o));
            }
            return command.ExecuteReader();
        }

        public void ExecuteNonQuery(string sqlQuery, IEnumerable<object> parameters)
        {
            var command = new SQLiteCommand(sqlQuery, m_dbConnection);
            string paramName = "@param";
            int paramNr = 1;
            foreach (object o in parameters)
            {
                string parNam = paramName + paramNr;
                paramNr++;
                if (!sqlQuery.Contains(parNam)) throw new PxSQLException("Query does not contain a parameter with this name.");
                command.Parameters.Add(new SQLiteParameter(parNam, o));
            }
            ExecuteNonQuery(command);
        }

        private void ExecuteNonQuery(string sqlQuery) => ExecuteNonQuery(new SQLiteCommand(sqlQuery, m_dbConnection));

        private void ExecuteNonQuery(SQLiteCommand command) => command.ExecuteNonQuery();

        public string GetQuery(string controlName, string queryName) => queries[controlName][queryName];

        public void Dispose()
        {
            m_dbConnection.Dispose();
        }

        private class PxSQLException : IOException
        {
            public PxSQLException() : this("Database error.") { }
            public PxSQLException(string str) : base(str) { }
        }
    }
}
