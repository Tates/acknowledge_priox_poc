﻿using Newtonsoft.Json;
using Priox.Core.Client.Models.UIComponents;
using Priox.Core.Models.Interfaces.UIComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Client.WebAPI.Tools
{
    public static class APITools
    {
        private static HttpClient client = new HttpClient();

        public static async Task<T> GetObjectAsync<T>(string path) where T : class, new()
        {
            T result = default(T);
            string typ = typeof(T).ToString();
            string json = string.Empty;
            using (HttpResponseMessage response = await client.GetAsync(path))
                if (response.IsSuccessStatusCode)
                {
                    json = await response.Content.ReadAsStringAsync();
                    if (typeof(T) == typeof(PxWindow))
                        return (T)ParseWindow(json);
                    return JsonConvert.DeserializeObject<T>(json);
                }
            return result;
        }

        public static async Task<string> GetStringAsync(string path)
        {
            string result = default(string);
            using (HttpResponseMessage response = await client.GetAsync(path))
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<string>();
                }
            return result;
        }

        /// <summary>
        /// Unfortunately interfaces may need to be parsed manually.
        /// You can parse any object to any class as long as it has all the required fields. 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private static object ParseWindow(string json)
        {
            string childrenJson = json.Substring(json.IndexOf("\"Children\":") + "\"Children\":".Length);
            childrenJson = $"{childrenJson.Substring(0, childrenJson.LastIndexOf("]") + 1)}";
            IEnumerable<KeyValuePair<string, PxUberComponent>> components = JsonConvert.DeserializeObject<KeyValuePair<string, PxUberComponent>[]>(childrenJson);
            PxWindow window = JsonConvert.DeserializeObject<PxWindow>(json.Replace(childrenJson, "[]"));
            window.Children = new PxComponentCollection();
            foreach (var pair in components)
                window.Children.Add(pair.Value.GetComponent);
            object o = window;
            return window;
        }

        private class PxUberComponent : IPxTextBox
        {
            public string Text { get; set; }

            public ComponentType Type { get; set; }

            public int X { get; set; }
            public int Y { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string Name { get; set; }

            public char PasswordChar { get; set; }

            public PxComponentCollection Children { get; }
            public IPxComponent GetComponent
            {
                get
                {
                    switch (Type)
                    {
                        case ComponentType.Window:
                            return new PxWindow()
                            {
                                Text = Text,
                                X = X,
                                Y = Y,
                                Width = Width,
                                Height = Height,
                                Name = Name
                            };
                        case ComponentType.Button:
                            return new PxButton(Name, X, Y, Width, Height, Text);
                        case ComponentType.TextBox:
                            return new PxTextBox()
                            {
                                Text = Text,
                                X = X,
                                Y = Y,
                                Width = Width,
                                Height = Height,
                                Name = Name,
                                PasswordChar = PasswordChar
                            };
                        case ComponentType.Component:
                        default:
                            return this;
                    }
                }
            }

            public void Dispose() { }
        }

    }
}
