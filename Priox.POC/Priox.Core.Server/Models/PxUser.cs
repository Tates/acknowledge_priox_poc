﻿using Priox.Core.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Server.Models
{
    public class PxUser : IPxUser
    {
        public string UserName { get; }
        public PxUserLevel Level { get; }
        public PxUser(string userName, PxUserLevel level)
        {
            UserName = userName;
            Level = level;
        }
    }
}
