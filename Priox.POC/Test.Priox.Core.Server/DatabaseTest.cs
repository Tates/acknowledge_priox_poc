﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Core.Server
{
    [TestClass]
    public class DatabaseTest
    {
        [TestMethod]
        public void TestGetQuery()
        {
            Priox.Core.Server.Database.PxDatabaseConnect db = new Priox.Core.Server.Database.PxDatabaseConnect();
            string result = db.GetQuery("Register", "RegisterUser");
            Assert.AreEqual("INSERT INTO PxUser (user_name, password_hash, salt, user_level) values (@param1, @param2, @param3, @param4)", result);
        }

        [TestMethod]
        public void TestGetSalt()
        {
            byte[] result;
            using (Priox.Core.Server.Database.Screens.LoginScreenDB screen = new Priox.Core.Server.Database.Screens.LoginScreenDB())
            {
                result = screen.GetSalt("admin");
            }
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestLogin()
        {
            Priox.Core.Models.Interfaces.IPxUser result;
            using (Priox.Core.Server.Database.Screens.LoginScreenDB screen = new Priox.Core.Server.Database.Screens.LoginScreenDB())
            {
                var salt = screen.GetSalt("admin");
                var hash = Priox.Core.Logic.EncriptionTools.GenerateSaltedHash(Encoding.UTF8.GetBytes("password"), salt);
                result = screen.Login("admin", hash);
            }
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestLoginFailNoUser()
        {
            try
            {
                using (Priox.Core.Server.Database.Screens.LoginScreenDB screen = new Priox.Core.Server.Database.Screens.LoginScreenDB())
                {
                    var salt = screen.GetSalt("adam");
                    Assert.IsNull(salt);
                }
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void TestLoginFailWrongPass()
        {
            Priox.Core.Models.Interfaces.IPxUser result;
            using (Priox.Core.Server.Database.Screens.LoginScreenDB screen = new Priox.Core.Server.Database.Screens.LoginScreenDB())
            {
                var salt = screen.GetSalt("admin");
                var hash = Priox.Core.Logic.EncriptionTools.GenerateSaltedHash(Encoding.UTF8.GetBytes("wrong password"), salt);
                result = screen.Login("admin", hash);
            }
            Assert.IsNull(result);
        }
    }
}
