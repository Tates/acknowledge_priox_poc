﻿using Priox.Core.Models.Interfaces;
using Priox.Core.Models.Interfaces.UIComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priox.Core.Client.Screens
{
    public interface ILoginScreen
    {
        IPxWindow Window { get; }
        Task<IPxUser> LoginAsync(string userName, string password);
        Task<IPxUser> GetUser(string userName, byte[] passwordHash);
        Task<byte[]> GetSaltAsync(string userName);
    }
}
